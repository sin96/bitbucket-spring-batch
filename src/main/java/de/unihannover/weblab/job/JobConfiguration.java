package de.unihannover.weblab.job;

import de.unihannover.weblab.tasks.popularmasterslave.PopularMasterslaveTasklet;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import de.unihannover.weblab.tasks.tagtagmasterslave.TagTagMasterslaveTasklet;

/**
 * This class implements the jobs for all batches.
 * It is important that the batch.properties sets
 * batch.job.configuration.package=de.unihannover.weblab.job
 * to lookup for Jobs (JavaJobs) in this package.
 *
 * Created by sinan on 26/11/15.
 */
@Configuration
public class JobConfiguration {

    @Autowired
    private JobBuilderFactory jobs;

    @Autowired
    private StepBuilderFactory steps;

    /**
     * Represent the "popularMasterslaveJob" which includes
     * PopularMasterslaveTasklet as a step.
     *
     * @return
     */
    @Bean (name = "popularMasterslaveJob")
    public Job popularMasterslaveJob() {

        PopularMasterslaveTasklet task = new PopularMasterslaveTasklet();
        Step popularMasterslaveJobStep = steps.get("popularMasterslaveJobStep")
                .tasklet(task)
                .build();

        return this.jobs.get("popularMasterslaveJob").incrementer(task).
                flow(popularMasterslaveJobStep).
                end().build();
    }

    /**
     * Represent the "tagTagMasterslaveJob" which includes
     * TagTagMasterslaveTasklet as a step.
     *
     * @return
     */
    @Bean (name = "tagTagMasterslaveJob")
    public Job tagTagMasterslaveJob() {

        TagTagMasterslaveTasklet task = new TagTagMasterslaveTasklet();
        Step popularMasterslaveJobStep = steps.get("tagTagMasterslaveJobStep")
                .tasklet(task)
                .build();

        return this.jobs.get("tagTagMasterslaveJob").incrementer(task).
                flow(popularMasterslaveJobStep).
                end().build();
    }
}
