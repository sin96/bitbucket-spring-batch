package de.unihannover.weblab.tasks.popularmasterslave.domain;

/**
 * Represent a pair object with two Strings.
 *
 * Created by sinan on 20/12/15.
 */
public class Pair {

    private String a;
    private String b;

    @Override
    public String toString() {
        return a + "; " + b;
    }

    public String getA() {
        return a;
    }

    public void setA(String a) {
        this.a = a;
    }

    public String getB() {
        return b;
    }

    public void setB(String b) {
        this.b = b;
    }
}
