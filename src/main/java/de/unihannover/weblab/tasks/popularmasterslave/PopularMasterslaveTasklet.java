package de.unihannover.weblab.tasks.popularmasterslave;

import de.unihannover.weblab.tasks.popularmasterslave.domain.Pair;
import de.unihannover.weblab.tasks.popularmasterslave.mappers.PopularMasterSlaveMapper;
import de.unihannover.weblab.service.MyBatisUtil;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.session.SqlSession;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersIncrementer;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * This class represents the batch job "batch_popular_masterslave.pl" from Bibsonomy.
 * If a variable is not set then the tasklet will run with the default variable.
 *
 * Documentation from perl script:
 *
 * This script fills the popular tables for bibtexs, bookmarks, and
 * tags. For each of the given number of days the top items are
 * extracted.
 *
 * Changes:
 *   2015-09-11 (dzo)
 *   - added option to exclude users from popular statistics
 *     (currently dblp, genealogie)
 *   2011-06-28 (rja)
 *   - module Common.pm contains helper functions; include it using
 *     export PERL5LIB=/home/bibsonomy/batch_scripts/
 *   - database configuration now done using Common.pm
 *   2011-06-27 (rja)
 *   - all database configuration variables are now read via
 *     environment variables (e.g., MASTER_HOST, MASTER_USER, ...)
 *   2008-04-22 (rja)
 *   - created master/slave version
 *   - changed from "last N rows" to "last N days"
 *   - added possibility to compute popularity for different
 *     number of days
 *   2009-02-16 (dbe)
 *   - added list of tags to be excluded from popular tags table
 *
 *
 * Created by sinan on 20/12/15.
 */
public class PopularMasterslaveTasklet implements Tasklet, JobParametersIncrementer {


    /**
     * DEFAULT VARIABLES
     *  Bookmark
        ##################################################################
        # configuration
        ##################################################################
        # these are arrays - enter as many days as blocks you like
     */
    private final static int[] LAST_BOOKMARK_DAYS = new int[]{7, 30, 120};
    private final static int[] LAST_BIBTEX_DAYS = new int[]{7, 30, 120};
    private final static int[] LAST_TAG_DAYS = new int[]{10};

    // # max number of posts for each day block
    private final static long MAX_BOOKMARKS = 100;
    private final static long MAX_BIBTEXS = 100;
    private final static long MAX_TAGS = 100;

    // # tags to be excluded from popular tags (lower case)
    private final static String[] TAGS_TO_EXCLUDE = new String[]{"imported", "jabref:nokeywordassigned"};
    // # users to be excluded from the statistics
    private final static String[] USERS_TO_EXCLUDE = new String[]{"dblp", "genealogie"};
    private final static String ROWS_BOOKMARK = "content_id,book_description,book_extended,book_url_hash,date,user_name,rating";

    /**
     * Publication
     */
    private static String ROWS_BIBTEX   = "content_id,journal,volume,chapter,edition,month,day,bookTitle,howPublished," +
            "institution,organization,publisher,address,school,series,bibtexKey,date,user_name,url,type,description,annote," +
            "note,pages,bKey,number,crossref,misc,bibtexAbstract,simhash0,simhash1,simhash2,simhash3,title,author,editor,year,entrytype,rating";

    // local variables
    private int[] last_bookmark_days = LAST_BOOKMARK_DAYS;
    private int[] last_bibtex_days = LAST_BIBTEX_DAYS;
    private int[] last_tag_days = LAST_TAG_DAYS;
    private long max_bookmarks = MAX_BOOKMARKS;
    private long max_bibtexs = MAX_BIBTEXS;
    private long max_tags = MAX_TAGS;
    private String[] tags_to_exclude = TAGS_TO_EXCLUDE;
    private String[] users_to_exclude = USERS_TO_EXCLUDE;
    private String rows_bookmark = ROWS_BOOKMARK;
    private String rows_bibtex = ROWS_BIBTEX;


    /**
     * The accompanying job will run this method.
     * In the ChunkContext are the job parameters.
     * This method initialize the connection to the database and runs all methods for the batch.
     *
     * @param stepContribution
     * @param chunkContext
     * @return
     * @throws Exception
     */
    @Override
    public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {
        Map<String, Object> jobParameters = chunkContext.getStepContext().getJobParameters();
        addJobParameter(jobParameters);

        SqlSession slave = MyBatisUtil.getSlaveSqlSessionFactory().openSession();
        SqlSession master = MyBatisUtil.getMasterSqlSessionFactory().openSession();
        try{
            executeBookmarkOrPublication(slave, master, "bookmark", this.last_bookmark_days, this.max_bookmarks,
                    this.rows_bookmark, "book_url_hash");
            executeBookmarkOrPublication(slave, master, "bibtex", this.last_bibtex_days, this.max_bibtexs,
                    this.rows_bibtex, "simhash1");
            tags(slave, master);
        }finally{
            slave.close();
            master.close();
        }
        return RepeatStatus.FINISHED;
    }

    /**
     * This method is only for debugging. It returns the SQL query with filled parameters.
     * @param id
     * @param parameters
     * @return
     */
    private BoundSql getStatement(String id, Map<String, Object> parameters){

        MappedStatement ms = MyBatisUtil.getSlaveSqlSessionFactory().getConfiguration().getMappedStatement(id);
        BoundSql boundSql = ms.getBoundSql(parameters);
        return boundSql;
    }

    /**
     * Dependent on the parameters this method execute the statement for bookmark or publication.
     * @param slave
     * @param master
     * @param resourcetype
     * @param last_resourcetype_days
     * @param max_resourcetype
     * @param rows_resourcetype
     * @param idAKey
     * @throws SQLException
     */
    private void executeBookmarkOrPublication(SqlSession slave, SqlSession master, String resourcetype,
                                              int[] last_resourcetype_days, long max_resourcetype, String rows_resourcetype,
                                              String idAKey) throws SQLException {

        //  # exclude certain user
        // TODO: in NOT IN
        String condition = "";
        for (String s : users_to_exclude)
            condition += " AND user_name != '" + s + "' ";

        // # clean table
        master.delete("delete_"+resourcetype);

        for (int days : last_resourcetype_days){

            System.out.println(resourcetype+", " + days + " days: ");

            // # get TOP Bookmarks/Bibtexs
            Map<String, String> parameters = new HashMap<>();
            parameters.put("max_"+resourcetype+"s", ""+ max_resourcetype);
            parameters.put("days", ""+days);
            parameters.put("condition", condition);

            List<Pair> ids = slave.selectList("select_"+resourcetype, parameters);
            slave.commit();

            int rank = 0;
            for (Pair id : ids){
                rank++;
                System.out.print(".");

                Map<String, String> map = new HashMap<>();
                map.put(idAKey, id.getA());
                map.put("rows_"+resourcetype, rows_resourcetype);

                Map first = slave.selectOne("get_first_"+resourcetype, map);

                if (first != null){
                    Map<String, Object> insertMap = new HashMap<>();
                    insertMap.put("rows_"+resourcetype, rows_resourcetype);

                    int i = 1;
                    for (String s : rows_resourcetype.split(",")){
                        Object value = first.get(s);

                        insertMap.put("in" + i, "\"" + value + "\"");
                        i++;
                    }
                    insertMap.put("ctr", id.getB());
                    insertMap.put("rank", String.valueOf(rank));
                    insertMap.put("days", String.valueOf(days));

                    master.insert("insert_"+resourcetype, insertMap);
                }
            }
            System.out.println("");
        }

        slave.commit();
        master.commit();
    }

    /**
     * execute statements (BibTeX)
     * @param slave
     * @param master
     */
    private void tags(SqlSession slave, SqlSession master) {
        PopularMasterSlaveMapper popularMasterSlaveMapper_slave = slave.getMapper(PopularMasterSlaveMapper.class);
        PopularMasterSlaveMapper popularMasterSlaveMapper_master = master.getMapper(PopularMasterSlaveMapper.class);

        /**
         * ##########################################
         # execute statements (tags)
         # for each content type we calculate the
         # most popular tags
         ##########################################
         */
        // # clean table
        popularMasterSlaveMapper_master.delete_tags();

        //# all, bookmark, bibtex
        int[] content_types = {0,1,2};

        // # loop over different content types

        for (int content_type : content_types){
            System.out.println("tags, content_type = " + content_type);

            String condition = "";
            if (content_type == 0) {
                // # all content types
                condition = "";
            } else {
                // # bookmark, bibtex, ...
                condition = " AND content_type = "+content_type+" ";
            }

            // # exclude certain tags
            for (String exclude_tag : this.tags_to_exclude) {
                condition += " AND tag_lower != '"+exclude_tag+"' ";
            }

            // # exclude certain users
            for (String exclude_user : this.users_to_exclude) {
                condition += " AND user_name != '"+exclude_user+"' ";
            }

            // # get last tag rows, order them by ctr

            // # insert rows
            for (int days : this.last_tag_days) {
                System.out.print(days + " days: ");

                // # get TOP bookmarks
                Map<String, String> parameters = new HashMap<>();
                parameters.put("max_tags", ""+this.max_tags);
                parameters.put("days", ""+days);
                parameters.put("condition", condition);
                List<Pair> rows = popularMasterSlaveMapper_slave.select_tags(parameters);
                slave.commit();

                // # get tags
                for (Pair row : rows){
                    System.out.print(".");
                    Map<String, String> insertMap = new HashMap<>();
                    insertMap.put("a", row.getA());
                    insertMap.put("b", row.getB());
                    insertMap.put("content_type", ""+content_type);
                    insertMap.put("days", ""+days);
                    popularMasterSlaveMapper_master.insert_tags(insertMap);
                }

            }
            System.out.println("");
            slave.commit();
            master.commit();
        }
    }


    /**
     * Add the job parameters for the next execution.
     * If a job parameter doesn't exists then take the default parameter.
     * This method have to be run before execution of the statements in the execute method.
     * @param jobParameters
     */
    private void addJobParameter(Map<String, Object> jobParameters) {

        this.max_bookmarks = jobParameters.containsKey("max_bookmarks") ? (long) jobParameters.get("max_bookmarks") : MAX_BOOKMARKS;
        this.max_bibtexs = jobParameters.containsKey("max_bibtexs") ? (long) jobParameters.get("max_bibtexs") : MAX_BIBTEXS;
        this.max_tags = jobParameters.containsKey("max_tags") ? (long) jobParameters.get("max_tags") : MAX_TAGS;

        if (jobParameters.containsKey("last_bookmark_days")){
            String param = (String) jobParameters.get("last_bookmark_days");
            String[] split = param.split(",");

            int[] last_bookmark_days = new int[split.length];
            int i = 0;
            for (String s : split)
                last_bookmark_days[i++] = Integer.parseInt(s);
            this.last_bookmark_days = last_bookmark_days;
        } else
            this.last_bookmark_days = LAST_BOOKMARK_DAYS;

        if (jobParameters.containsKey("last_bibtex_days")){
            String param = (String) jobParameters.get("last_bibtex_days");
            String[] split = param.split(",");

            int[] last_bibtex_days = new int[split.length];
            int i = 0;
            for (String s : split)
                last_bibtex_days[i++] = Integer.parseInt(s);
            this.last_bibtex_days = last_bibtex_days;
        } else
            this.last_bibtex_days = LAST_BIBTEX_DAYS;

        if (jobParameters.containsKey("last_tag_days")){
            String param = (String) jobParameters.get("last_tag_days");
            String[] split = param.split(",");

            int[] last_tag_days = new int[split.length];
            int i = 0;
            for (String s : split)
                last_tag_days[i++] = Integer.parseInt(s);
            this.last_tag_days = last_tag_days;
        } else
            this.last_tag_days = LAST_TAG_DAYS;

        if (jobParameters.containsKey("tags_to_exclude")){
            String param = (String) jobParameters.get("tags_to_exclude");
            String[] split = param.split(",");

            String[] tags_to_exclude = new String[split.length];
            int i = 0;
            for (String s : split)
                tags_to_exclude[i++] = s;
            this.tags_to_exclude = tags_to_exclude;
        } else
            this.tags_to_exclude = TAGS_TO_EXCLUDE;

        if (jobParameters.containsKey("users_to_exclude")){
            String param = (String) jobParameters.get("users_to_exclude");
            String[] split = param.split(",");

            String[] users_to_exclude = new String[split.length];
            int i = 0;
            for (String s : split)
                users_to_exclude[i++] = s;
            this.users_to_exclude = users_to_exclude;
        } else
            this.users_to_exclude = USERS_TO_EXCLUDE;

        this.rows_bookmark = jobParameters.containsKey("rows_bookmark") ? (String) jobParameters.get("rows_bookmark") : ROWS_BOOKMARK;
        this.rows_bibtex = jobParameters.containsKey("rows_bibtex") ? (String) jobParameters.get("rows_bibtex") : ROWS_BIBTEX;
    }

    /**
     * This method is the override of the JobParametersIncrementer interface.
     * This method sets the next job parameter based on the last run.
     * It sets a id which is the time, because the job parameter have to be unique.
     *
     * @param jobParameters
     * @return JobParameters
     */
    @Override
    public JobParameters getNext(JobParameters jobParameters) {
        long id = System.currentTimeMillis();
        String last_bookmark_days = "";
        for (int i = 0; i < this.LAST_BOOKMARK_DAYS.length; i++)
            last_bookmark_days += (i==0) ? this.LAST_BOOKMARK_DAYS[i] : ","+this.LAST_BOOKMARK_DAYS[i];
        String last_bibtex_days = "";
        for (int i = 0; i < this.LAST_BIBTEX_DAYS.length; i++)
            last_bibtex_days += (i==0) ? this.LAST_BIBTEX_DAYS[i] : ","+this.LAST_BIBTEX_DAYS[i];
        String last_tag_days = "";
        for (int i = 0; i < this.LAST_TAG_DAYS.length; i++)
            last_tag_days += (i==0) ? this.LAST_TAG_DAYS[i] : ","+this.LAST_TAG_DAYS[i];
        long max_bookmarks = this.MAX_BOOKMARKS;
        long max_bibtexs = this.MAX_BIBTEXS;
        long max_tags = this.MAX_TAGS;
        String tags_to_exclude = "";
        for (int i = 0; i < this.TAGS_TO_EXCLUDE.length; i++)
            tags_to_exclude += (i==0) ? this.TAGS_TO_EXCLUDE[i] : ","+this.TAGS_TO_EXCLUDE[i];
        String users_to_exclude = "";
        for (int i = 0; i < this.USERS_TO_EXCLUDE.length; i++)
            users_to_exclude += (i==0) ? this.USERS_TO_EXCLUDE[i] : ","+this.USERS_TO_EXCLUDE[i];
        String rows_bookmark = this.ROWS_BOOKMARK;
        String rows_bibtex = this.ROWS_BIBTEX;


        JobParameters parameters = new JobParametersBuilder()
                .addLong("max_bookmarks", jobParameters.getLong("max_bookmarks", max_bookmarks))
                .addLong("max_bibtexs", jobParameters.getLong("max_bibtexs", max_bibtexs))
                .addLong("max_tags", jobParameters.getLong("max_tags", max_tags))
                .addString("last_bookmark_days", jobParameters.getString("last_bookmark_days", last_bookmark_days))
                .addString("last_bibtex_days", jobParameters.getString("last_bibtex_days", last_bibtex_days))
                .addString("last_tag_days", jobParameters.getString("last_tag_days", last_tag_days))
                .addString("tags_to_exclude", jobParameters.getString("tags_to_exclude", tags_to_exclude))
                .addString("users_to_exclude", jobParameters.getString("users_to_exclude", users_to_exclude))
                .addString("rows_bookmark", jobParameters.getString("rows_bookmark", rows_bookmark))
                .addString("rows_bibtex", jobParameters.getString("rows_bibtex", rows_bibtex))
                .addLong("run.time", id)
                .addString("cron_expression", jobParameters.getString("cron_expression", ""))
                .toJobParameters();
        return parameters;
    }
}
